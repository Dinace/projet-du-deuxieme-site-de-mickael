//
// Modele pour l'objet javascript pizza.
//
class Pizza {
  constructor(id, image, name, details, price) {
    this.id = id
    this.image = image
    this.name = name
    this.details = details
    this.price = price
  }
}

//
// Services pour les pizzas.
//
const Pizzas = {
  getPizzas: () => {
    return JSON.parse(localStorage.getItem("pizzas"))
  },

  savePizzas: (pizzas) => {
    localStorage.setItem("pizzas", JSON.stringify(pizzas))
    return pizzas
  },

  removePizza: (pizzaId) => {
    const pizzas = Pizzas.getPizzas()
    const data = pizzas.filter((pizza) => pizza.id != pizzaId)
    return Pizzas.savePizzas(data)
  },
}

//
// Modele pour un utilisateur.
//
class Utilisateur {
  constructor(id, password) {
    this.id = id
    this.password = password
  }
}

// Services pour les utilisateurs.
const Utilisateurs = {
  getUtilisateurs: () => {
    return JSON.parse(localStorage.getItem("utilisateurs"))
  },

  saveUtilisateurs: (utilisateurs) => {
    localStorage.setItem("utilisateurs", JSON.stringify(utilisateurs))
    return utilisateurs
  },

  removeUtilisateurs: (utilisateurId) => {
    const utilisateurs = Utilisateurs.getUtilisateurs()
    const data = utilisateurs.filter((utilisateur) => utilisateur.id != utilisateurId)
    return Utilisateurs.saveUtilisateurs(data)
  },

  login: (id, password) => {
    // retrouver un utilisateur
    const utilisateurs = Utilisateurs.getUtilisateurs()
    const result = utilisateurs.filter((utilisateur) => {
      return utilisateur.id == id && utilisateur.password == password
    })
    // check du resultat
    console.log(result)
    if (result.length == 0) {
      alert("authentification non réussie")
    } else {
      localStorage.setItem("login", 1)
      alert("authentification réussie")
      window.location.replace("./admin.html")
    }
    return result
  },

  isLogin: () => {
    const result = localStorage.getItem("login")
    return result != null
  },

  logout: () => {
    localStorage.removeItem("login")
    window.location.replace("./index.html")
  },
}

//
// Charge le contenu du tableau pour les pizzas.
//
function populateTableList() {
  let content = ""
  let pizzas = Pizzas.getPizzas()

  for (let i = 0; i < pizzas.length; i++) {
    let pizza = pizzas[i]
    console.info(pizza)
    content += `
    <tr class="text-center">
    <td><img src=${pizza.image} class="img-fluid img-thumbnail w-50"></td>
    <td class="w-25 align-middle">${pizza.name}</td>
    <td class="w-25 align-middle">${pizza.price}</td>
    <td class="w-25 align-middle">
        <button 
            class="btn btn-info" 
            type="button" 
            data-toggle="collapse" 
            data-target="#${pizza.id}" 
            aria-expanded="false" 
            aria-controls="${pizza.id}">
            view
        </button>
        <div class="collapse" id="${pizza.id}">
            ${pizza.details}
        </div>
    </td>
    `
    if (Utilisateurs.isLogin()) {
      content += `
        <td class="w-25 align-middle">
          <button data-id=${pizza.id} class="supprimer btn btn-danger mt-2 mb-2">Supprimer</button>
        </td>
      `
    }
  }

  document.getElementById("productList").innerHTML = content

  // Attacher l'action supprimer à chaque boutton supprimer (class="supprimer")
  const buttonsSupprimer = document.getElementsByClassName("supprimer")
  for (let i = 0; i < buttonsSupprimer.length; i++) {
    // Action supprimer
    buttonsSupprimer[i].addEventListener("click", (event) => {
      const pizzaId = event.target.dataset.id
      Pizzas.removePizza(pizzaId)
      populateTableList()
    })
  }
}

//
// Reset des données
//
function reset() {
  init()
  populateTableList()
}

//
// Utile pour initialiser les données.
//
function init() {
  // Pizzas
  const pizza1 = new Pizza(
    "pizza-1",
    "img/pizza-royale.jpg",
    "Pizza Royale",
    "Ingrédients: tomate, jambon, olives,tomates,farine",
    20
  )
  const pizza2 = new Pizza(
    "pizza-2",
    "img/pizza-moza.jpg",
    "Pizza Moza",
    "Ingrédients: mozarella, oignons,sel,tomate, persil",
    12
  )
  const pizza3 = new Pizza(
    "pizza-3",
    "img/pizza-tomate.jpg",
    "Pizza Tomate",
    "Ingrédients: tomate en fruit, purée de tomate, sel, huile",
    25
  )
  const pizza4 = new Pizza(
    "pizza-4",
    "img/pizza-vege.jpg",
    "Pizza Vege",
    "Ingrédients: poivron, champignon, crème, ail",
    18
  )

  const pizzas = [pizza1, pizza2, pizza3, pizza4]
  Pizzas.savePizzas(pizzas)

  // Utilisateurs
  const admin = new Utilisateur("toto", "123")
  Utilisateurs.saveUtilisateurs([admin])
}

//
// Init
//
const pizzas = Pizzas.getPizzas()
const utilisateurs = Utilisateurs.getUtilisateurs()
if (!pizzas || !utilisateurs) {
  init()
  populateTableList()
  console.log("intialisation des données réussie")
} else {
  populateTableList()
  console.log("intialisation des données déjà réalisée")
}

//
// Login
//
document.getElementById("registration").addEventListener("submit", (formulaire) => {
  formulaire.preventDefault()
  const form = document.getElementById("registration")
  const login = form.login.value
  const password = form.password.value
  Utilisateurs.login(login, password)
})
